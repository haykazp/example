$(document).ready(function(){

    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf("/") + 1);

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/user/'+id,
        dataType: 'JSON',
        success: function (response) {
            $("#first_name").append(response.first_name);
            $("#last_name").append(response.last_name);
        }
    });


    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/product',
        dataType: 'JSON',
        success: function (response) {
            var len = response.length;
            for(var i=0; i<len; i++){
                var id = response[i].id;
                var name = response[i].name;
                var price = response[i].price;
                var count = response[i].count;
                var code = response[i].code;

                var tr_str = "<tr>" +
                    "<td align='center'>" + id + "</td>" +
                    "<td align='center'>" + name + "</td>" +
                    "<td align='center'>" + price + "</td>" +
                    "<td align='center'>" + count + "</td>" +
                    "<td align='center'>" + code + "</td>" +
                    "<td align='center'>" + "<a href='/api/user/"+ id +"'>Update</a> " + "<a href='/api/user/"+ id +"'>Delete</a> " + "</td>" +
                    "</tr>";

                $("#products").append(tr_str);


            }
        }
    });
});
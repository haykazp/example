package servlet.api.id;

import com.google.gson.Gson;
import domain.Product;
import domain.User;
import service.Implementation.ProductDaoImplementation;
import service.Implementation.UserDaoImplementation;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ProductIdServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String param = request.getPathInfo().replace("/","");

        ProductDaoImplementation productDaoImplementation = new ProductDaoImplementation();
        Product product = productDaoImplementation.getById(Long.parseLong(param));

        response.setContentType("application/json; charset=utf-8");

        PrintWriter writer = response.getWriter();
        writer.print(new Gson().toJson(product));
        writer.flush();
    }
}
